class Calc

    def self.add(x, y)
      x + y
    end

    def Calc.subtract(x, y)
      x - y
    end

    def Calc.multiply(x, y)
      x * y
    end

    def Calc.divide(x, y)
      if (y != 0)
        x / y
      else
        # TODO: throw error for division by zero? does it even matter??
        0
      end
    end

end
