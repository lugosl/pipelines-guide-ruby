require "calc"

describe Calc do

  describe ".add" do
    context "given 3 and 3" do
      it "returns 6" do
        expect(Calc.add(3, 3)).to eql(6)
      end
    end
  end

  describe ".subtract" do
    context "given 3 and 3" do
      it "returns 0" do
        expect(Calc.subtract(3, 3)).to eql(0)
      end
    end
  end

  describe ".multiply" do
    context "given 3 and 3" do
      it "returns 9" do
        expect(Calc.multiply(3, 3)).to eql(9)
      end
    end
  end

  describe ".divide" do
    context "given 3 and 3" do
      it "returns 1" do
        expect(Calc.divide(3, 3)).to eql(1)
      end
    end
  end

end
